import { DecisionTree } from './decision_tree';
import { entropy, collectionEntropy } from './entropy';
import { matches } from './matches';
import { _ } from 'lodash';
import { ID3 } from './id3';


let tree = new ID3();
let prediction = tree.predict([
  {first_team: 'fnatic', second_team: 'sarrope', map: 'de_dust2'},
  {first_team: 'fnatic', second_team: 'Ninjas in Pyjamas', map: 'de_dust2'},
  {first_team: 'fnatic', second_team: 'Ninjas in Pyjamas', map: 'de_mirage'},
  {first_team: 'fnatic', second_team: 'Ninjas in Pyjamas', map: 'de_overpass'},
  {first_team: 'fnatic', second_team: 'Ninjas in Pyjamas', map: 'de_inferno'},
]);
console.log(prediction);

prediction = tree.predict([
  {second_team: 'fnatic', first_team: 'sarrope', map: 'de_dust2'},
  {second_team: 'fnatic', first_team: 'Ninjas in Pyjamas', map: 'de_dust2'},
  {second_team: 'fnatic', first_team: 'Ninjas in Pyjamas', map: 'de_mirage'},
  {second_team: 'fnatic', first_team: 'Ninjas in Pyjamas', map: 'de_overpass'},
  {second_team: 'fnatic', first_team: 'Ninjas in Pyjamas', map: 'de_inferno'},
]);
console.log(prediction);

