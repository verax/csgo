import fs from 'fs';

export class DecisionTree {
  constructor() {
    this.features = JSON.parse(fs.readFileSync('features.json', 'utf-8'));
    this.labels   = JSON.parse(fs.readFileSync('labels.json', 'utf-8'));
  }

  log () {
    console.log(this.features);
  }
}
