import fs from 'fs';
import _ from 'lodash';
import { entropy, collectionEntropy } from './entropy';
import { matches } from './matches';

class Node {
  constructor(data) {
    this.key = null;
    this.vals = data;
    this.branches = {};
  }

  setKey (key) {
    this.key = key;
  }

  leaf () {
    return Object.keys(this.branches).length === 0;
  }
}

class NodeFromJSON {
  constructor(data) {
    this.key = data.key;
    this.vals = data.vals;
    this.branches = {};
    _.keys(data.branches).forEach(key => {
      this.branches[key] = new NodeFromJSON(data.branches[key]);
    });
  }

  leaf () {
    return Object.keys(this.branches).length === 0;
  }
}
// NOTES: winner column is a class because that characteristic decide whether a team wins or not.
//        procedimiento para seleccionar un atributo particular para ser un nodo de decisión de un árbol de decisión
export class ID3 {
  constructor(build = false) {
    this.collection        = matches();
    this.winners           = _.map(this.collection, 'winner');
    this.collectionEntropy = collectionEntropy(this.winners, 10);
    this.root              = new Node(this.collection);
    if (build) {
      this.build(this.root);
      fs.writeFileSync('tree.json', JSON.stringify(this.root, null, 2), 'utf-8');
    } else {
      let json = JSON.parse(fs.readFileSync('tree.json', 'utf-8'));
      this.root = new NodeFromJSON(json);
    }
  }

  build(node) {
    let data = node.vals;
    if(data.length === 0) {
      throw new Error('No data!!!');
      return;
    }; 
    let keys    = _.keys(data[0]); 
    _.remove(keys, k => k === 'winner');
    if(keys.length === 0) {
      return;
    };


    let gains = [];
    keys.forEach(key => {
      let gain = this.informationGain(this.collection, key, this.collectionEntropy);
      gains = gains.concat({key, gain});
    });
    let maxGain = _.max(gains, 'gain');
    let key = maxGain.key;
    node.setKey(key); 
    let branches = _.uniq(_.map(data, maxGain.key));

    branches.every(branch => { 
      var copy = _.map(data, _.clone);
      let branchData = _.select(copy, b => b[maxGain.key] === branch);
      branchData = branchData.map(d => {delete d[maxGain.key]; return d});
      if (branchData.length > 0) {
        let newNode = new Node(branchData);
        node.branches[branch] = newNode;
        this.build(newNode);
      } else {
        if (node.vals.size > 1) {
          console.log(JSON.stringify(node, null, 2));
        }
      }
      return true;
    });
  }

  predict (values) {
    let results = [];

    values.forEach(val => {
      let result = this.search(val);
      results = results.concat(result)
      console.log(`${val.first_team} vs ${val.second_team} in ${val.map} -> ${result}`);
    });

    return results;
  }

  search (data) {
    let node = this.root;
    while(node && !node.leaf()) {
      let branch = data[node.key];
      let newNode = node.branches[branch];
      if (!newNode) {
        let winnersCount = _.countBy(this.winners);
        if ((winnersCount[data.first_team] || 0) > (winnersCount[data.second_team] || 0)) {
          return data.first_team;
        } else {
          return data.second_team;
        }
      }
      node = newNode;
    }

    let winnersCount = _.countBy(_.map(node.vals, 'winner'));
    if ((winnersCount[data.first_team] || 0) > (winnersCount[data.second_team] || 0)) {
      return data.first_team;
    } else {
      return data.second_team;
    }
  }

  informationGain(data, column, entropyS) {
    let gain = 0;

    let values = _.map(data, column);
    let valuesCount  = _.countBy(values, x => x);

    for(let key in valuesCount) {
      gain += (valuesCount[key] / values.length) * entropy(values, key, 10);
    }
    return entropyS - gain;
  }
}
