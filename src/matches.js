import fs from 'fs';

export class Match {
  constructor({team1, team2, map, winner}) {
    this.first_team = this._sanitize(team1);
    this.second_team = this._sanitize(team2);
    this.map = this._sanitize(map);
    this.winner = this._sanitize(winner);
  }

  _sanitize (val) {
    return val.replace(/"/g, '').replace(/\s+/, ' ');
  }

  swapedTeams () {
    return {
      first_team: this.second_team,
      second_team: this.first_team,
      map: this.map,
      winner: this.winner
    }
  }
}

export function matches () {
  let data, size, matches;

  data = fs.readFileSync('matches.csv', 'utf-8').split("\n");
  size = data.length - 1;
  data = data.slice(1, size);
  matches = [];

  data.forEach(row => {
    var match;
    let [, team1, team2, map, winner, ] = row.split(',');
    match = new Match({team1, team2, map, winner});
    matches = matches.concat([match, match.swapedTeams()]);
  });

  return matches;
}
