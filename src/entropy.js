import { _ } from 'lodash';

export function collectionEntropy (values, b = 2) {
  let n, entropy, count, valuesCount;

  n           = values.length;
  valuesCount = _.countBy(values, w => w);
  entropy     = 0;

  for (let value in valuesCount) {
    let p = valuesCount[value] / n;

    entropy += p*log2(p, b);
  }

  return -entropy;
}

export function entropy (values, val, b = 2) {
  let counts, p;

  counts = _.countBy(values, v => v);
  p      = counts[val] / values.length;

  return -p*log2(p, b);
}

function log2(n, b = 2) {
  return Math.log(n) / Math.log(b);
}
